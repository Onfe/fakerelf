const faker = require('faker');
const fetch = require('node-fetch');
const util = require('util');

const HOST_URL = 'http://localhost:8080/collect';

class FakerElf {
  constructor(seed) {
    this.seed = seed || 1234;
    faker.seed(this.seed);
    this.name = `FakerElf-${faker.internet.mac().replace(/:/g, '')}`;
    this.startTime = Date.now();

    // water is usually cooler than the air temp.
    this.airTemperature = faker.random.number({min: 10, max: 30, precision: 0.1});
    this.waterTemperature = faker.random.number({
      min: Math.floor(this.airTemperature) - 4,
      max: Math.floor(this.airTemperature) + 1,
      precision: 0.1
    });
    this.temperatureTrend = faker.random.arrayElement([-1, 0, 1]);

    this.humidity = faker.random.number({max: 100, precision: 0.01});

    // Fish produce ammonia, raising the pH slightly above 7.
    this.pH = faker.random.number({min: 6, max: 9, precision: 0.1});
    this.light = faker.random.number(200);

    const bedCount = 3;
    this.beds = [];
    for (let i = 0; i < bedCount; i++) {
      this.beds[i] = {
        status: faker.random.number(3),
        level: faker.random.number({min: 10, max: 60, precision: 0.1})
      }
    }

    this.rssi = faker.random.number({max: -30, min: -75});
  }

  generatePayload() {
    const pl = {
      timestamp: Date.now() - this.startTime,
      waterTemp: this.round(this.waterTemperature, 2),
      airTemp: this.round(this.airTemperature, 2),
      humidity: this.round(this.humidity, 1),
      light: this.round(this.light, 0),
      pH: this.round(this.pH, 2),
      rssi: this.round(this.rssi),
      beds: this.beds
    }

    return JSON.stringify(pl);
  }

  round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  noise(magnitude) {
    return faker.random.number({min: -magnitude, max: magnitude, precision: 0.01});
  }

  send(payload) {
    fetch(
      HOST_URL + `/${this.name}`,
      {
        method: 'POST',
        body: payload,
        headers: { 'Content-Type': 'application/json' }
      }
    ).then(res => {
      console.log(`${res.status}: ${payload}`);
      if (res.status == 200) {
        res.json().then(cmds => {
          console.log(` ↳ RECIEVED COMMANDS!`);
          cmds.forEach(cmd => console.log(`   - ${util.inspect(cmd)}`));
        });
      }
    }).catch(err => {
      console.error(`[KO] ${err.message}`);
    });
  }

  update() {
    this.airTemperature += this.noise(0.1);
    this.waterTemperature += this.noise(0.1);
    this.humidity += this.noise(2);
    this.light += this.noise(2);
    
    this.beds.forEach(bed => {
      bed.status = faker.random.number(3) > 2 ? (bed.status + 1) % 4 : bed.status;
      bed.level += this.noise(1);
      bed.level = this.round(bed.level)
    })
  }

  start() {
    if (this.interval) return;

    this.interval = setInterval(
      () => {
        let pl = this.generatePayload();
        this.send(pl);
        this.update();
      },
      5000
    );
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }
}

const elf = new FakerElf();

elf.start();